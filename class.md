### Class #5

#### Bioinformática Aplicada às Ciências da Vida 2023-2024

##### The Omics Revolution: Metagenomics

<img src="assets/logo-FCUL.svg" style="background:none; border:none; box-shadow:none;">


© Francisco Pina Martins 2023

---

### Summary

* What is metagenomics
* Metagenomics paradigms
* Amplicon metagenomics
* Hands on

---

### What is metagenomics?

<div style="float:left; width:70%;">

* &shy;<!-- .element: class="fragment" -->Application of sequencing techniques to DNA from microbial communities
* &shy;<!-- .element: class="fragment" -->Unlike "traditional" genomics (focused on individual genomes), explores the collective genomes of microbial communities
    * &shy;<!-- .element: class="fragment" -->Allows detecting the whole diversity of microorganisms
    * &shy;<!-- .element: class="fragment" -->Allows detecting the whole diversity of genes
* &shy;<!-- .element: class="fragment" -->Studies composition, functionality, and dynamics of complex ecosystems

</div>
<div style="float:right; width:30%;" class="fragment">

</br>
</br>

![Microorganisms](assets/microorganisms.jpg)

</div>

---

### Applications of Metagenomics

<div style="float:left; width:70%;">

* &shy;<!-- .element: class="fragment" -->Often to detect and quantify microorganisms present in a sample
* &shy;<!-- .element: class="fragment" -->Environmental Microbiology
    * &shy;<!-- .element: class="fragment" -->Soil, water, air, and extreme environments
    * &shy;<!-- .element: class="fragment" -->Impact on ecosystem processes and biodiversity
* &shy;<!-- .element: class="fragment" -->Human Microbiome Research
    * &shy;<!-- .element: class="fragment" -->Role in health and disease
* &shy;<!-- .element: class="fragment" -->Biotechnology
    * &shy;<!-- .element: class="fragment" -->Identifying novel enzymes & pathways
    * &shy;<!-- .element: class="fragment" -->Assess potential biotechnological applications

</div>
<div style="float:right; width:30%;" class="fragment">

</br>
</br>

<a title="DataBase Center for Life Science (DBCLS), CC BY 4.0 &lt;https://creativecommons.org/licenses/by/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:202004_Gut_microbiota.svg"><img alt="202004 Gut microbiota" src="assets/gut_microbiota.svg"></a>

</div>

---

### Metagenomics Challanges

<div style="float:left; width:50%;">

</br>
</br>
</br>
</br>

* Data complexity
* Sampling strategies
* &shy;<!-- .element: class="fragment highlight-green" -->Bioinformatics hurdles

</div>
<div style="float:right; width:50%;" class="fragment">

![Overcoming obstacles](assets/obstacles.jpg)

</div>

---

### Metagenomics paradigms

<div style="float:left; width:50%;">

#### Shotgun Metagenomics

* &shy;<!-- .element: class="fragment" -->Sequences all genetic material in a sample 
    * &shy;<!-- .element: class="fragment" -->All taxa are sequenced
    * &shy;<!-- .element: class="fragment" -->All genes are sequenced
* &shy;<!-- .element: class="fragment" -->Unbiased
* &shy;<!-- .element: class="fragment" -->Can study both diversity and function
* &shy;<!-- .element: class="fragment" -->Requires high quantity of DNA
* &shy;<!-- .element: class="fragment" -->Expensive!

</div>
<div style="float:right; width:50%;">

#### Amplicon Metagenomics

* &shy;<!-- .element: class="fragment" -->Sequences specific markers
    * &shy;<!-- .element: class="fragment" -->16S (bacteria)
    * &shy;<!-- .element: class="fragment" -->18S (eukaryotes)
    * &shy;<!-- .element: class="fragment" -->ITS (fungi)
* &shy;<!-- .element: class="fragment" -->Targeted
* &shy;<!-- .element: class="fragment" -->Can only characterize communities
* &shy;<!-- .element: class="fragment" -->Requires very little DNA
* &shy;<!-- .element: class="fragment" -->Cheap!

</div>
<div style="float:right; width:100%;" class="fragment">

![Green arrow](assets/arrow.png)

</div>

|||

### Metagenomics paradigms (workflow)

<div style="float:left; width:50%;">

#### Shotgun Metagenomics

* &shy;<!-- .element: class="fragment" -->DNA extraction from sample
* &shy;<!-- .element: class="fragment" -->DNA fragmentation
* &shy;<!-- .element: class="fragment" -->HTS sequencing
* &shy;<!-- .element: class="fragment" -->Bioinformatics tasks are assembling and classifying these fragments
    * &shy;<!-- .element: class="fragment" -->Taxonomic assignment
    * &shy;<!-- .element: class="fragment" -->Functional assignment
    * &shy;<!-- .element: class="fragment" -->Difficult & long

</div>
<div style="float:right; width:50%;">

#### Amplicon Metagenomics

* &shy;<!-- .element: class="fragment" -->DNA extraction from sample
* &shy;<!-- .element: class="fragment" -->Specific PCR amplification
* &shy;<!-- .element: class="fragment" -->HTS sequencing
* &shy;<!-- .element: class="fragment" -->Bioinformatics tasks identify and merge sequenced amplicons
    * &shy;<!-- .element: class="fragment" -->Taxonomic assignment
    * &shy;<!-- .element: class="fragment" -->Also difficult (but less), and faster

</div>

<div style="float:right; width:100%;" class="fragment">

![Green arrow](assets/arrow.png)

</div>

---

<div style="float:left; width:50%;">

### Amplicon metagenomics analysis

* &shy;<!-- .element: class="fragment" -->Once sequences are obtained, data analysis can begin
    * &shy;<!-- .element: class="fragment" -->Data QC
    * &shy;<!-- .element: class="fragment" -->Read merging
    * &shy;<!-- .element: class="fragment" -->Denoising/Clustering
    * &shy;<!-- .element: class="fragment" -->Quantifying
    * &shy;<!-- .element: class="fragment" -->Taxonomy assignment

</div>
<div style="float:right; width:50%;" class="fragment">

![Amplicon pipeline](assets/amplicon_pipeline.jpg)

</div>

|||

### Data QC

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->As usual, sequencing reads needs a thorough clean-up
    * &shy;<!-- .element: class="fragment" -->Truncation
    * &shy;<!-- .element: class="fragment" -->Quality filtering
    * &shy;<!-- .element: class="fragment" -->Size filtering
    * &shy;<!-- .element: class="fragment" -->Discard underrepresented samples
* &shy;<!-- .element: class="fragment" -->Once this is done, overlapping paired reads are merged

</div>
<div style="float:right; width:50%;" class="fragment">

![Amplicon pipeline](assets/cleaning-service.jpg)

</div>

|||

### Alignment

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Read contigs are then matched to a reference database
    * &shy;<!-- .element: class="fragment" -->[Silva](https://www.arb-silva.de/)
    * &shy;<!-- .element: class="fragment" -->[RDP](https://lcsciences.com/documents/sample_data/16S_sequencing/src/html/top1.html)
    * &shy;<!-- .element: class="fragment" -->[Greengenes](https://greengenes.secondgenome.com/)
* &shy;<!-- .element: class="fragment" -->Discard reads that:
    * &shy;<!-- .element: class="fragment" -->Have poor alignment score
    * &shy;<!-- .element: class="fragment" -->Are shifted in their position (relative to other reads)
* &shy;<!-- .element: class="fragment" -->*Chimeric* contigs are deleted

</div>
<div style="float:right; width:50%;" class="fragment">

[![Amplicon pipeline](assets/Chimera.jpg)](https://en.wikipedia.org/wiki/Chimera_(mythology))

</div>

|||

### Clustering and consensus

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Similar reads are then clustered and counted
* &shy;<!-- .element: class="fragment" -->Consensus reads are extracted for each [OTU](https://doi.org/10.1007/s12223-018-0627-y)
* &shy;<!-- .element: class="fragment" -->Results are summarized
* &shy;<!-- .element: class="fragment" -->OUT counts are then used to calculate output metrics

</div>
<div style="float:right; width:50%;" class="fragment">

![Gift-wrapped](assets/gift_wrapped.webp)

</div>

---

### Metagenomics outputs

[![Metagenomics outputs](assets/mg_outputs.jpg)](https://doi.org/10.1007/s13238-020-00724-8)

|||

### Metagenomics outputs

* &shy;<!-- .element: class="fragment" -->Abundance: Proportion of OTU individuals relative to all individuals
    * &shy;<!-- .element: class="fragment" -->Bar plots
* &shy;<!-- .element: class="fragment" -->Alpha Diversity: Measure of *within sample* diversity
    * &shy;<!-- .element: class="fragment" -->Shannon index
    * &shy;<!-- .element: class="fragment" -->Boxplots
* &shy;<!-- .element: class="fragment" -->Beta Diversity: Measure of *between samples* diversity
    * &shy;<!-- .element: class="fragment" -->Bray-Curtis distance
    * &shy;<!-- .element: class="fragment" -->PCoA
* &shy;<!-- .element: class="fragment" -->Taxonomic diversity: Infer evolutionary relationships between OTUs
    * &shy;<!-- .element: class="fragment" -->UPGMA
    * &shy;<!-- .element: class="fragment" -->Phylogenetic trees
* &shy;<!-- .element: class="fragment" -->Feature correlation: How OTUs correlate to a certain feature
    * &shy;<!-- .element: class="fragment" -->Correlation analysis
    * &shy;<!-- .element: class="fragment" -->Scatter plots

---

### Hands on

* &shy;<!-- .element: class="fragment" -->We will now analyse a 16s amplicon dataset from [this paper](https://doi.org/10.3389/fpls.2020.00599)
    * &shy;<!-- .element: class="fragment" -->How are plant microbiomes affected by drought?
    * &shy;<!-- .element: class="fragment" -->Three sample types, across 5 species and 3 conditions!
    * &shy;<!-- .element: class="fragment" -->366 samples
* &shy;<!-- .element: class="fragment" -->Due to time constrains we will only use 12 samples
    * &shy;<!-- .element: class="fragment" -->Subsampled to a mere 80000 reads per sample

---

### Your tools for today

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Resort to 5 different programs
    * &shy;<!-- .element: class="fragment" -->[Dada2](https://benjjneb.github.io/dada)
    * &shy;<!-- .element: class="fragment" -->[Mothur](http://mothur.org/)
    * &shy;<!-- .element: class="fragment" -->[Vsearch](https://github.com/torognes/vsearch)
    * &shy;<!-- .element: class="fragment" -->[FastTree](http://www.microbesonline.org/fasttree/)
    * &shy;<!-- .element: class="fragment" -->[Phyloseq](https://bioconductor.org/packages/release/bioc/html/phyloseq.html)
* &shy;<!-- .element: class="fragment" -->All have to be installed
* &shy;<!-- .element: class="fragment" -->Analysis pipeline has 32 steps with these programs

</div>
<div style="float:right; width:50%;" class="fragment">

![Overwhelming](assets/overwhelmed.jpg)

</div>

---

### Automation FTW

* &shy;<!-- .element: class="fragment" -->[Metaflow|mics](https://metagenomics-pipelines.readthedocs.io/en/latest/index.html)
* &shy;<!-- .element: class="fragment" -->Pipeline built in [Nextflow](https://www.nextflow.io/)
    * &shy;<!-- .element: class="fragment" -->Automates the process
* &shy;<!-- .element: class="fragment" -->Uses programs from [Docker](https://docker.com)
    * &shy;<!-- .element: class="fragment" -->Isolated environment containing all necessary software

&shy;<!-- .element: class="fragment" -->[![PBRC](assets/PBRC_header.png)](https://www.pbrc.hawaii.edu/)

---

### First steps

* &shy;<!-- .element: class="fragment" -->Login to *Darwin* and let's get to work
    * &shy;<!-- .element: class="fragment" -->*Hint*: Start a `screen` session for optimal results!
* &shy;<!-- .element: class="fragment" -->Getting MetaFlow|mics is just one `git clone` away:

<div class="fragment">

```bash
cd ~/
git clone https://github.com/hawaiidatascience/metaflowmics
```

</div>

* &shy;<!-- .element: class="fragment" -->Next we edit some config files

<div class="fragment">


```bash
cd ~/metaflowmics/metaflowmics
nano Pipeline-16S/nextflow.config
```

</div>

* &shy;<!-- .element: class="fragment" -->Change `min_overlap` value to = 0
* &shy;<!-- .element: class="fragment" -->Change `clustering_thresholds` value to `"99"`
* &shy;<!-- .element: class="fragment" -->Change `beta_diversity` value to `"braycurtis-thetayc-sharedsobs-sharedchao"`
* &shy;<!-- .element: class="fragment" -->What did we just change?

---

### Running the pipeline

* &shy;<!-- .element: class="fragment" -->Once we are happy with our configuration, we start the pipeline

<div class="fragment">

```bash
cd ~/metaflowmics/metaflowmics
nextflow run Pipeline-16S -profile docker --reads "/home/data/meta/drought1/*_R{1,2}.fastq.gz" --referenceAln /home/data/databases/silva.nr_v138_1.align --referenceTax /home/data/databases/silva.nr_v138_1.tax --outdir ~/drought_res --cpus 3 -w /scratch/bacv/fc29609
```

</div>

* &shy;<!-- .element: class="fragment" -->Look at all these switches!
    * &shy;<!-- .element: class="fragment" -->run: The pipeline we want to run
    * &shy;<!-- .element: class="fragment" -->profile: Use the programs from a docker container
    * &shy;<!-- .element: class="fragment" -->reads: FASTQ files location in the filesystem
    * &shy;<!-- .element: class="fragment" -->reference*: Silva database location in the filesystem (you're welcome!)
    * &shy;<!-- .element: class="fragment" -->outdir: Where to save the outputs
    * &shy;<!-- .element: class="fragment" -->cpus: Number of CPU cores to use
    * &shy;<!-- .element: class="fragment" -->w: set the working directory for intermediate files. Don't forget to change to your student number!
* &shy;<!-- .element: class="fragment" -->Get a coffee this will take ~15 minutes to finish

---

### First results

* &shy;<!-- .element: class="fragment" -->The pipeline is done, but we are not!
* &shy;<!-- .element: class="fragment" -->Use python to take a look at the first plots

<div class="fragment">

```bash
cd ~/drought_res
python3 -m http.server 29609  # Don't forget to use your own student number!
```

</div>

* &shy;<!-- .element: class="fragment" -->Point your browser to *Darwin*'s address and have a close look at out abundance plots:
    * &shy;<!-- .element: class="fragment" -->barplot-Class_99.html
    * &shy;<!-- .element: class="fragment" -->barplot-Phylum_99.html
    * &shy;<!-- .element: class="fragment" -->clustermap_99.html

---

### Post processing and diversity indices

* &shy;<!-- .element: class="fragment" -->To obtain our alpha and beta diversity plots we need to do some data wrangling
* &shy;<!-- .element: class="fragment" -->First let's add a column with the sample type to our alpha-diversity file
    * &shy;<!-- .element: class="fragment" -->Consider taking a look at this file with `batcat` first

<div class="fragment">


```bash
cd ~/drought_res/postprocessing
cut -f 2 alpha-diversity.99.summary | cut -d "_" -f 1 |sed 's/group/type/' | paste alpha-diversity.99.summary - > alpha-div.tsv
```

</div>

* &shy;<!-- .element: class="fragment" -->We also need to make some changes to the beta diversity file
* &shy;<!-- .element: class="fragment" -->In this case we need to correct the tabs which are quite bugged!
    * &shy;<!-- .element: class="fragment" -->Once again, consider taking a look at this file with `batcat` before manipulating it

<div class="fragment">

```bash
sed 's/\t\t/\t/' beta-diversity.99.summary | sed 's/comparison/comparision1\tcomparison2/' > beta-div.tsv
```

</div>

* &shy;<!-- .element: class="fragment" -->Copy the resulting files to your local machine

---

### The final plots

* &shy;<!-- .element: class="fragment" -->Use [this script](assets/pcoa.R) to calculate and plot a PCoA from the beta diversity data
    * &shy;<!-- .element: class="fragment" -->You have to change the path to your case
    * &shy;<!-- .element: class="fragment" -->Add a `png()` and the respective `dev.off()` if you want to run the script on *Darwin*
* &shy;<!-- .element: class="fragment" -->Draw a box plot with the Shannon diversity indices (SOIL and RHIZO)
    * &shy;<!-- .element: class="fragment" -->Perform a Wilcoxon test to assess whether there are significant differences between Shannon diversity indices of SOIL and RHIZO samples
    * &shy;<!-- .element: class="fragment" -->This part I leave entirely up to you (it should come naturally by now)!
    * &shy;<!-- .element: class="fragment" -->*Hint*: The Wilcoxon test also works with the 'formula' notation, just like the `boxplot()` function. Try it!

---

### References

* [A practical guide to amplicon and metagenomic analysis of microbiome data](https://doi.org/10.1007)
* [MetaFlow](https://metagenomics-pipelines.readthedocs.io/en/latest/)
* [Drought Drives Spatial Variation in the Millet Root Microbiome]( https://doi.org/10.3389/fpls.2020.00599)
* [Introduction to Microbiome Data Science](https://microbiome.github.io/course_2021_radboud/)
